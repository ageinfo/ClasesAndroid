package com.vbg.clasesandroid.animations;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.transitionseverywhere.TransitionManager;
import com.vbg.clasesandroid.R;

/**
 * Created by victo on 20/07/2017.
 */

/**
 * Animaciones.
 * <p>
 * Ejemplos de animaciones mas complejas:
 *
 * @see <a href="https://github.com/lgvalle/Material-Animations"></a>
 * <p>
 * Libreria RevealLayout:
 * @see <a href="https://github.com/HendraAnggrian/reveallayout"
 */
public class AnimationsActivity extends AppCompatActivity implements ListFragment.SampleListProvider {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_layout);
        ListFragment listFragment = new ListFragment();
        listFragment.setSampleListListener(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, listFragment)
                .commit();
    }

    @Override
    public void onSampleSelected(int index) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out,
                        R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.container, createFragmentForPosition(index))
                .addToBackStack(String.valueOf(index))
                .commit();
    }

    @Override
    public int getSampleCount() {
        return 11;
    }

    @Override
    public String getTitleForPosition(int index) {
        switch (index) {
            case 0:
                return "Simple animations with AutoTransition";
            case 1:
                return "Interpolator, duration, start delay";
            case 2:
                return "Path motion";
            case 3:
                return "Slide transition";
            case 4:
                return "Scale transition";
            case 5:
                return "ChangeImageTransform transition";
            case 8:
                return "Recolor transition";
            case 9:
                return "Rotate transition";
            case 10:
                return "Change text transition";
        }
        return null;
    }

    private Fragment createFragmentForPosition(int index) {
        switch (index) {
            case 0:
                return new AutoTransitionSample();
            case 1:
                return new InterpolatorDurationStartDelaySample();
            case 2:
                return new PathMotionSample();
            case 3:
                return new SlideSample();
            case 4:
                return new ScaleSample();
            case 5:
                return new ImageTransformSample();
            case 8:
                return new RecolorSample();
            case 9:
                return new RotateSample();
            case 10:
                return new ChangeTextSample();
        }
        return null;
    }

}

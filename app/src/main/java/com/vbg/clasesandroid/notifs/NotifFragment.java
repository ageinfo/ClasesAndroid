package com.vbg.clasesandroid.notifs;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.vbg.clasesandroid.MainActivity;
import com.vbg.clasesandroid.R;

/**
 * Created by victo on 01/06/2017.
 */

public class NotifFragment extends Fragment {


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        View rootView = inflater.inflate(R.layout.notifications_fragment, container, false);

        Button notificationSimple = (Button) rootView.findViewById(R.id.simple_not);
        notificationSimple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNotification(false);
            }
        });

        Button bigNot = (Button) rootView.findViewById(R.id.big_not);
        bigNot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayNotification();
            }
        });

        Button headsUp = (Button) rootView.findViewById(R.id.headsUp);
        headsUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNotification(true);
            }
        });

        return rootView;
    }

    private void addNotification(boolean headsUp) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.drawable.ic_male)
                        .setContentTitle("Título")
                        .setContentText("Esto es una notificación de ejemplo")
                        .setDefaults(Notification.DEFAULT_ALL); /*DEFAULT_ALL: vibracion, tono notificacion*/

        if (headsUp)
            builder.setPriority(Notification.PRIORITY_MAX);

        /*click*/
        Intent notificationIntent = new Intent(getContext(), MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getContext(), 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        /*show*/
        NotificationManager manager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    protected void displayNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext());
        mBuilder.setContentTitle("Mensaje nuevo");
        mBuilder.setContentText("Ha recibido un nuevo mensaje");
        mBuilder.setSmallIcon(R.drawable.ic_female);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        String[] events = new String[6];
        events[0] = "Linea #1";
        events[1] = "Linea #2";
        events[2] = "Linea #3";
        events[3] = "Linea #4";

        inboxStyle.setBigContentTitle("Detalles:");

        for (String event : events) {
            inboxStyle.addLine(event);
        }

        mBuilder.setStyle(inboxStyle);

        Intent resultIntent = new Intent(getContext(), MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(getContext(), 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(contentIntent);

        NotificationManager manager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, mBuilder.build());
    }
}

package com.vbg.clasesandroid.controls.hourview;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 06/07/2017.
 */

public class HourViewFragment extends Fragment {

    View rootView;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.hourview_fragment, container, false);

        final Calendar calendar = Calendar.getInstance();
        final int hourToday = calendar.get(Calendar.HOUR_OF_DAY);
        final int minuteToday = calendar.get(Calendar.MINUTE);
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);


        Button button = (Button) rootView.findViewById(R.id.btnHour);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), onTimeSetListener, hourToday, minuteToday,  true);
                timePickerDialog.show();
            }
        });

        Button button1 = (Button) rootView.findViewById(R.id.btnCalendar);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), dateListener, year, month, day);

                /*Calendar cc = Calendar.getInstance();
                cc.add(Calendar.MONTH, -1);
                datePickerDialog.getDatePicker().setMinDate(cc.getTimeInMillis());*/

                datePickerDialog.show();

            }
        });
        return rootView;
    }

    private TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {
            Toast.makeText(getContext(), "Hora seleccionada: " + i + ":" + i1, Toast.LENGTH_SHORT).show();
        }
    };

    private DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            Toast.makeText(getContext(), "FechaSeleccionada: " + arg3 + "/" + (arg2 + 1) + "/" + arg1, Toast.LENGTH_SHORT).show();


        }
    };

}
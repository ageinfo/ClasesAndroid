package com.vbg.clasesandroid.controls.switchfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import io.ghyeok.stickyswitch.widget.StickySwitch;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 06/04/2017.
 */

public class SwitchFragment extends Fragment {

    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.switch_fragment_layout, container, false);

        Switch simpleSwitch = (Switch) rootView.findViewById(R.id.simpleSwitch);
        simpleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getContext(), isChecked ? "Seleccionado" : "Deseleccionado", Toast.LENGTH_SHORT).show();

            }
        });

        StickySwitch stickySwitch = (StickySwitch) rootView.findViewById(R.id.sticky_switch);
        stickySwitch.setShowText(true);
        stickySwitch.setTextColor(0x000000);
        stickySwitch.setOnSelectedChangeListener(new StickySwitch.OnSelectedChangeListener() {
            @Override
            public void onSelectedChange(StickySwitch.Direction direction, String text) {
                Toast.makeText(getContext(), "Seleccionado : " + direction.name() + ", Texto : " + text, Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }
}

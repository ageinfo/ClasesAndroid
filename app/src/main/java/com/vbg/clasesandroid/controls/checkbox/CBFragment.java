package com.vbg.clasesandroid.controls.checkbox;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 17/03/2017.
 */

public class CBFragment extends Fragment {

    View rootView;
    CheckBox cb1, cb2, cb3;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.checkbox_fragment, container, false);

        cb1 = (CheckBox) rootView.findViewById(R.id.checkbox1);
        cb2 = (CheckBox) rootView.findViewById(R.id.checkbox2);
        cb3 = (CheckBox) rootView.findViewById(R.id.checkbox3);

        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getActivity(), "CheckBox 1 " + (isChecked?"seleccionado":"deseleccionado"), Toast.LENGTH_SHORT).show();
            }
        });

        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getActivity(), "CheckBox 2 " + (isChecked?"seleccionado":"deseleccionado"), Toast.LENGTH_SHORT).show();
            }
        });

        cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getActivity(), "CheckBox 3 " + (isChecked?"seleccionado":"deseleccionado"), Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

}
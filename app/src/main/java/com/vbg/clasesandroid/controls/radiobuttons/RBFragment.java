package com.vbg.clasesandroid.controls.radiobuttons;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 17/03/2017.
 */

public class RBFragment extends Fragment {

    View rootView;
    RadioGroup rg1;
    RadioButton rb1, rb2, rb3;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.radio_group_fragment, container, false);

        rb1 = (RadioButton) rootView.findViewById(R.id.rb1);
        rb2 = (RadioButton) rootView.findViewById(R.id.rb2);
        rb3 = (RadioButton) rootView.findViewById(R.id.rb3);
        rg1 = (RadioGroup) rootView.findViewById(R.id.rg);


        rg1.clearCheck();
        rb2.setChecked(true);
        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb1:
                        showToast("RadioButton 1");
                        break;
                    case R.id.rb2:
                        showToast("RadioButton 2");
                        break;
                    case R.id.rb3:
                        showToast("RadioButton 3");
                        break;
                    default:
                        break;
                }
            }
        });
        return rootView;
    }

    private void showToast(String msg) {
        Toast.makeText(getActivity(), "Opción seleccionada: " + msg, Toast.LENGTH_SHORT).show();
    }

}

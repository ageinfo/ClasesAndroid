package com.vbg.clasesandroid.controls.spinner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 06/04/2017.
 */

public class SpinnerFragment extends Fragment {

    View rootView;
    Spinner spinner, spinner1;

    String[] valuesXml;
    String[] values;

    AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (view.getId()) {
                case R.id.spinner:
                    Toast.makeText(getContext(), "Item seleccionado: " + values[position], Toast.LENGTH_SHORT).show();
                    break;
                case R.id.spinner2:
                    Toast.makeText(getContext(), "Item seleccionado: " + valuesXml[position], Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.spinner_fragment_layout, container, false);

        spinner = (Spinner) rootView.findViewById(R.id.spinner);
        spinner1 = (Spinner) rootView.findViewById(R.id.spinner2);

        values = new String[]{"Código 1", "Código 2", "Código 3", "Código 4", "Código 5"};

        //EL adapter es el que muestra el contenido
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, values);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); //POner la flecha, esta es la por defecto

        //R.array = apunta a los arrays que haya en strings.xml de res
        //R.string apunta al recurso en si (o a los arrays)
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(), R.array.array_values, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        valuesXml = getContext().getResources().getStringArray(R.array.array_values);

        spinner.setAdapter(adapter);
        spinner1.setAdapter(adapter2);

        //opcion 1
        /*spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), "Item seleccionado: " + values[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getContext(), "Nada seleccionado", Toast.LENGTH_SHORT).show();
            }
        });

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), "Item seleccionado: " + valuesXml[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getContext(), "Nada seleccionado", Toast.LENGTH_SHORT).show();
            }
        });*/


        //opcion 2
        spinner.setOnItemSelectedListener(itemSelectedListener);
        spinner1.setOnItemSelectedListener(itemSelectedListener);

        return rootView;
    }

}
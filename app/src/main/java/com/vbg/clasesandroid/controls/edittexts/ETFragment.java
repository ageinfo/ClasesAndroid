package com.vbg.clasesandroid.controls.edittexts;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 06/04/2017.
 */

public class ETFragment extends Fragment {

    View rootView;

    TextInputLayout textInputLayout, textInputLayout2, textInputLayout3;
    EditText editText, editText2, editText3;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.edittext_fragment_layout, container, false);

        textInputLayout = (TextInputLayout) rootView.findViewById(R.id.textinput);
        textInputLayout2 = (TextInputLayout) rootView.findViewById(R.id.textinput2);
        textInputLayout3 = (TextInputLayout) rootView.findViewById(R.id.textinput3);

        editText = (EditText) rootView.findViewById(R.id.edittext);
        editText2 = (EditText) rootView.findViewById(R.id.edittext2);
        editText3 = (EditText) rootView.findViewById(R.id.edittext3);

        Button button = (Button) rootView.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateClick();
            }
        });

        setErrorHandler();
        return rootView;
    }

    public void validateClick() {
        textInputLayout.setErrorEnabled(true);
        textInputLayout2.setErrorEnabled(true);
        textInputLayout3.setErrorEnabled(true);

        textInputLayout.setError("Error 1");
        textInputLayout2.setError("Error 2");

        if (editText3.getText().length() > 10) {
            textInputLayout3.setError("Error 3"); //opcion 1  no deja escribir más de 10
        }
    }

    private void setErrorHandler() {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                 /*Sirve para cuando esribes algo se quita el error*/
                textInputLayout.setError(null);
                textInputLayout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputLayout2.setError(null);
                textInputLayout2.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //opcion 2 no deja escribir más de 10 y corta
                if (s.length() > 10) {
                    textInputLayout3.setErrorEnabled(true);
                    textInputLayout3.setError("No max 10");
                    editText3.setText(s.subSequence(0,10)); //corttado
                    editText3.setSelection(10);
                } else {
                    textInputLayout3.setError(null);
                    textInputLayout3.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}

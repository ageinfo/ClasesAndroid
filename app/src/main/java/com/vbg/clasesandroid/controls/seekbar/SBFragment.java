package com.vbg.clasesandroid.controls.seekbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.vbg.clasesandroid.R;
import com.vbg.clasesandroid.controls.seekbar.colorpicker.ColorPickerDialog;
import com.vbg.clasesandroid.controls.seekbar.colorpicker.Utils;

/**
 * Created by victo on 17/03/2017.
 */

public class SBFragment extends Fragment {


    View rootView;
    SeekBar sb;
    TextView tv, minTv, maxTv;

    CrystalRangeSeekbar rangeSeekbar;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.seek_bar_fragment, container, false);

        sb = (SeekBar) rootView.findViewById(R.id.seekBar);
        tv = (TextView) rootView.findViewById(R.id.tv_sb);

        rangeSeekbar = (CrystalRangeSeekbar) rootView.findViewById(R.id.rangeSeekbar);
        minTv=(TextView)rootView.findViewById(R.id.minTv);
        maxTv=(TextView)rootView.findViewById(R.id.maxTv);


        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tv.setText("Valor: " + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minTv.setText(String.valueOf(minValue));
                maxTv.setText(String.valueOf(maxValue));
            }
        });
        rangeSeekbar.setGap(30);
        rangeSeekbar.setSteps(10);
        //rangeSeekbar.setLeftThumbDrawable( R.drawable.cuatro); //KO?
        /*
         corner_radius: corner radius to be used seekbar, default 0f
         min_value: minimum value of seekbar, default 0
         max_value: maximum value of seekbar, default 100
         min_start_value: minimum start value must be equal or greater than min value, default min_value
         max_start_value: maximum start value must be equal or less than max value, default max_value
         steps: minimum steps between range, default NO_STEP -1f
         gap: maintain minimum range between dos thumbs, range must be greater >= min value && <= max value, default 0f
         fix_gap: maintain fix range between dos thumbs, range must be greater >= min value && <= max value, default NO_FIXED_GAP -1f
         bar_color inactive bar background color, default Color.GRAY
         bar_highlight_color active bar background color, default Color.BLACK
         left_thumb_color default left thumb color, default Color.BLACK
         left_thumb_color_pressed active left thumb color, default Color.DKGRAY
         left_thumb_image left thumb drawable, default null
         left_thumb_image_pressed active left thumb drawable, default null
         right_thumb_color default right thumb color, default Color.BLACK
         right_thumb_color_pressed active right thumb color, default Color.DKGRAY
         right_thumb_image right thumb drawable, default null
         right_thumb_image_pressed active right thumb drawable, default null
         position can be left or right, default left
         */

        return rootView;
    }


}
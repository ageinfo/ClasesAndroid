package com.vbg.clasesandroid.controls.fab;

import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.vbg.clasesandroid.R;

/**
 * Created by victo on 06/04/2017.
 */

public class FabFragment extends Fragment {

    View rootView;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.fab_fragment_layout, container, false);

        android.support.design.widget.FloatingActionButton floatingActionButton = (android.support.design.widget.FloatingActionButton) rootView.findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Varón", Toast.LENGTH_SHORT).show();
            }
        });

        android.support.design.widget.FloatingActionButton floatingActionButton2 = (android.support.design.widget.FloatingActionButton) rootView.findViewById(R.id.fab2);
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Hembra", Toast.LENGTH_SHORT).show();
            }
        });

        FloatingActionButton actionC = new FloatingActionButton(getContext());
        actionC.setTitle("C");
        actionC.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.accent)));
        actionC.setSize(FloatingActionButton.SIZE_MINI);
        actionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "FAB C presionado", Toast.LENGTH_SHORT).show();
            }
        });

        final FloatingActionsMenu menuMultipleActions = (FloatingActionsMenu) rootView.findViewById(R.id.multiple_actions);
        menuMultipleActions.addButton(actionC);


        FloatingActionButton actionA = (FloatingActionButton) rootView.findViewById(R.id.action_a);
        actionA.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.accent)));
        actionA.setSize(FloatingActionButton.SIZE_MINI);
        actionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "FAB A presionado", Toast.LENGTH_SHORT).show();
            }
        });

        FloatingActionButton actionB = (FloatingActionButton) rootView.findViewById(R.id.action_b);
        actionB.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.accent)));
        actionB.setSize(FloatingActionButton.SIZE_MINI);
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "FAB B presionado", Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }
}

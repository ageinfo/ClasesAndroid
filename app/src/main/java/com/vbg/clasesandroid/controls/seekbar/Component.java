package com.vbg.clasesandroid.controls.seekbar;

/**
 * Created by victo on 06/04/2017.
 */

public enum Component {
    BAR_COLOR, CONNECTING_LINE_COLOR, PIN_COLOR, THUMB_COLOR_PRESSED, SELECTOR_COLOR, TICK_COLOR, TEXT_COLOR
}

package com.vbg.clasesandroid.controls.interfaces.cardview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 17/03/2017.
 */

public class CDFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cardview_fragment, container, false);

    }
}

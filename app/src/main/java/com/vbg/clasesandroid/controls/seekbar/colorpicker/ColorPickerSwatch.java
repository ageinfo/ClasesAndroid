package com.vbg.clasesandroid.controls.seekbar.colorpicker;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.vbg.clasesandroid.R;

/**
 * Creates a circular swatch of a specified color.  Adds a checkmark if marked as checked.
 */
public class ColorPickerSwatch extends FrameLayout implements View.OnClickListener {

    private int mColor;

    private ImageView mSwatchImage;

    private ImageView mCheckmarkImage;

    private OnSwatchColorSelectedListener mOnSwatchColorSelectedListener;

    /**
     * Interface for a callback when a color square is selected.
     */
    public interface OnSwatchColorSelectedListener {

        /**
         * Called when a specific color square has been selected.
         */
        public void onSwatchColorSelected(int color);
    }

    public ColorPickerSwatch(Context context, int color, boolean checked,
                             OnSwatchColorSelectedListener listener) {
        super(context);
        mColor = color;
        mOnSwatchColorSelectedListener = listener;
        LayoutInflater.from(context).inflate(R.layout.color_picker_swatch, this);
        mSwatchImage = (ImageView) findViewById(R.id.color_picker_swatch);
        mCheckmarkImage = (ImageView) findViewById(R.id.color_picker_checkmark);
        setColor(color);
        setChecked(checked);
        setOnClickListener(this);
    }

    protected void setColor(int color) {
        Drawable[] colorDrawable = new Drawable[]
                {ContextCompat.getDrawable(getContext(), R.drawable.color_picker_swatch)};
        mSwatchImage.setImageDrawable(new ColorStateDrawable(colorDrawable, color));
    }

    private void setChecked(boolean checked) {
        if (checked) {
            mCheckmarkImage.setVisibility(View.VISIBLE);
        } else {
            mCheckmarkImage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (mOnSwatchColorSelectedListener != null) {
            mOnSwatchColorSelectedListener.onSwatchColorSelected(mColor);
        }
    }
}
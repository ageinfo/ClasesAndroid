package com.vbg.clasesandroid.controls.calendar;

import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 06/07/2017.
 */

public class CalendarFragment extends Fragment {

    View rootView;
    View barratop;
    CalendarView calendarView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.calendar_fragment, container, false);

        calendarView = (CalendarView) rootView.findViewById(R.id.simpleCalendarView);

        barratop = (View) rootView.findViewById(R.id.barratop);
        barratop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendarView.getVisibility() == View.VISIBLE)
                {
                    calendarView.setVisibility(View.GONE);
                }
                else
                {
                    calendarView.setVisibility(View.VISIBLE);
                }
            }
        });



        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Toast.makeText(getContext(), "Fecha seleccionada: " + dayOfMonth + "/" + (month + 1) + "/" + year, Toast.LENGTH_SHORT).show();
            }
        });

        /*
        Añadimos un límite superior al calendario. En este caso le añadimos un mes utilizando la funcion add()
        de la clase Calendar y se lo pasamos al calendarView en formato int como los milisegundos.
         */
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        calendarView.setMaxDate(calendar.getTimeInMillis());

        /*
        Añadimos un límite inferior al calendario. En este caso le quitamos un mes utilizando la funcion add()
        de la clase Calendar y se lo pasamos al calendarView en formato int como los milisegundos.
         */
        /*Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        calendarView.setMinDate(calendar.getTimeInMillis());*/



        return rootView;
    }

}

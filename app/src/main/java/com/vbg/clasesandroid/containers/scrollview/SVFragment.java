package com.vbg.clasesandroid.containers.scrollview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vbg.clasesandroid.R;

/**
 * Created by victo on 16/03/2017.
 */

public class SVFragment  extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scroll_view_fragment_horizontal, container, false);

    }
}

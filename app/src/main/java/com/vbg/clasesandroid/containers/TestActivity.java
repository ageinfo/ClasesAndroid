package com.vbg.clasesandroid.containers;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.vbg.clasesandroid.R;
import com.vbg.clasesandroid.containers.relativelayout.RLFragment;

import butterknife.ButterKnife;

/**
 * Created by Administrador on 05/05/2017.
 */

public class TestActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.fragment_act);

        //setResult(2);

        //Crear fragment 1
        Fragment fragment = null;
        fragment = new RLFragment();

        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        //transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        transaction.replace(R.id.fragmentid, fragment);
        //transaction.addToBackStack(null);
        transaction.commit();
    }
}

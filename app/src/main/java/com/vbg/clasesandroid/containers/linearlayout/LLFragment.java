package com.vbg.clasesandroid.containers.linearlayout;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vbg.clasesandroid.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by victo on 16/03/2017.
 */

public class LLFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.linear_layout_fragment, container, false);

    }
}

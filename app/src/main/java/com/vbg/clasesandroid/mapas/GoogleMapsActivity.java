package com.vbg.clasesandroid.mapas;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.vbg.clasesandroid.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victo on 06/07/2017.
 */

public class GoogleMapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    /**
     * Primero tenemos que configurar GoogleMaps en nuestra consola de APIs (https://console.developers.google.com/) para
     * obtener una clave que nos permita utilizar GoogleMaps
     * <p>
     * Una vez dentro tenemos que crear un proyecto nuevo (al lado del logo de GoogleAPIs)
     * <p>
     * Una vez creado, nos aparecerán todas las APIs de Google que podemos utilizar. Para utilizar
     * GoogleMaps en android tenemos que activar "Google Maps Android API"
     * <p>
     * Una vez habilitado nos dirigimos a Credenciales. Creamos una credencial "Clave de API" y copiamos la Clave que nos da
     * <p>
     * <p>
     * Una vez hecho esto añadimos GoogleMaps a nuestras dependencias: compile 'com.google.android.gms:play-services-maps:10.0.1'
     * y escribimos las siguientes 3 etiquetas dentro del <application></application> del Manifest (configuración de GoogleMaps)
     * <p>
     * <meta-data
     * android:name="com.google.android.gms.version"
     * android:value="@integer/google_play_services_version" />
     * <p>
     * <meta-data
     * android:name="com.google.android.geo.API_KEY"
     * android:value="AIzaSyAHC5mufoBU46bcG5FFNGX9uN_msF5-7NA"/>
     * <p>
     * Esta es una clave que he creado yo para este proyecto. Pero habría que poner la clave real.
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gmaps_activity);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLocationManager.removeUpdates(this);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });

        SupportMapFragment supportMapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        //return;
        //En este punto la app nos muestra el mapa global (sin zoom en nuestra posicion). Vamos a poner nuestra ubicación y realizar zoom en ella

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("MapActivity", "location granted");
            //Esta comprobación de permisos es inutil ya que solo entramos en esta Activity cuando se nos conceden los permisos, pero es obligatoria.
        }

        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);

        Location location = getLastKnownLocation();

        //final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude()); //En emulador casca pq. location es nulo
        final LatLng latLng = new LatLng(41.427828, 2.142034);

        //En este momento tenemos que la camara se ha movido hacia nuestra posicion
        //Vamos a añadir algun marcador, por ejemplo en el Vall d'Hebron

        final ArrayList<Marker> markers = new ArrayList<>();
        LatLng aa = new LatLng(41.427828, 2.142034); //latitude, logintud
        LatLng a1 = new LatLng(43.427828, 2.142034); //latitude, logintud

        Marker marker = googleMap.addMarker(new MarkerOptions().position(aa)
                .title("Vall d'Hebron").snippet("Esto es un hospital").icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).flat(false));
        markers.add(marker);

        //Agregar otro
        /*Marker marker2 = googleMap.addMarker(new MarkerOptions().position(a1)
                .title("Vall d'Hebron").snippet("Esto es un hospital").icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).flat(false));*/
        //markers.add(marker2);

        //img propia
        /*Marker marker = googleMap.addMarker(new MarkerOptions().position(aa)
                .title("Vall d'Hebron").snippet("Esto es un hospital").icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.city)).flat(false));*/


        /* Tambien podemos crear figuras geometricas
        * Siempre el ultimo punto debe ser igual al primero, para cerrarlo
        */

        PolylineOptions rectOptions = new PolylineOptions()
                .add(new LatLng(41.427828, 2.142034))
                .add(new LatLng(41.437828, 2.142034))
                .add(new LatLng(41.437828, 2.162034))
                .add(new LatLng(41.427828, 2.162034))
                .add(new LatLng(41.427828, 2.142034));

        Polyline polyline = googleMap.addPolyline(rectOptions);

        /*
        Y muchas ma sopciones: https://developers.google.com/maps/documentation/android-api/
         */


        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                moveCameraToShowAllMarkers(googleMap, markers, latLng);
            }
        });
    }

    /*
    Funcion para hacer el zoom necesario para mostrar una lista de marcadores junto a tu propia posicion
     */
    private void moveCameraToShowAllMarkers(final GoogleMap googleMap, ArrayList<Marker> markers, LatLng currentPos) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        if (currentPos != null)
            builder.include(currentPos);

        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }

        LatLngBounds bounds = builder.build();

        CameraUpdate cu;
        int padding = 200;

        if (markers.size() == 1 && currentPos == null) {
            cu = CameraUpdateFactory.newLatLngZoom(markers.get(0).getPosition(), 16F); //10F = zoom
        } else if (markers.size() == 0 && currentPos != null) {
            cu = CameraUpdateFactory.newLatLngZoom(currentPos, 16F);
        } else {
            cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        }

        googleMap.animateCamera(cu);


    }

    LocationManager mLocationManager;

    private Location getLastKnownLocation() {

        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("Location", "not granted");
            }
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}

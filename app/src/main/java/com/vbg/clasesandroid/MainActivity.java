package com.vbg.clasesandroid;

import android.Manifest;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.vbg.clasesandroid.animations.AnimationsActivity;
import com.vbg.clasesandroid.containers.linearlayout.LLFragment;
import com.vbg.clasesandroid.containers.relativelayout.RLFragment;
import com.vbg.clasesandroid.containers.scrollview.SVFragment;
import com.vbg.clasesandroid.controls.calendar.CalendarFragment;
import com.vbg.clasesandroid.controls.checkbox.CBFragment;
import com.vbg.clasesandroid.controls.edittexts.ETFragment;
import com.vbg.clasesandroid.controls.fab.FabFragment;
import com.vbg.clasesandroid.controls.hourview.HourViewFragment;
import com.vbg.clasesandroid.controls.interfaces.cardview.CDFragment;
import com.vbg.clasesandroid.controls.interfaces.progressbar.PBFragment;
import com.vbg.clasesandroid.controls.radiobuttons.RBFragment;
import com.vbg.clasesandroid.controls.seekbar.SBFragment;
import com.vbg.clasesandroid.controls.spinner.SpinnerFragment;
import com.vbg.clasesandroid.controls.switchfragments.SwitchFragment;
import com.vbg.clasesandroid.intents.IntentsActivity;
import com.vbg.clasesandroid.interfface.BottomBarActivity;
import com.vbg.clasesandroid.interfface.NavDrawer;
import com.vbg.clasesandroid.interfface.RecyclerViewGridClass;
import com.vbg.clasesandroid.interfface.TabsActivity;
import com.vbg.clasesandroid.mapas.GoogleMapsActivity;
import com.vbg.clasesandroid.notifs.NotifFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    /**
     * Con esta simple línea linkamos el objeto con id toolbar a la variable toolbar de clase Toolbar
     * <p>
     * Esto se realiza con una librería llamada ButterKnife {http://jakewharton.github.io/butterknife/} y que nos ahorra los findViewById().
     * <p>
     * Siempre hay que poner ButterKnife.bind(this) despues del setContentView
     */

    @Bind(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;
    @Bind(R.id.coordinator)
    CoordinatorLayout coordinator;
    PrimaryDrawerItem relativeItem;
    Drawer result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupDrawer();
        contacts();
        //changeScreen(1);

        //Lanzar normal
        //Intent intent = new Intent(this, TestActivity.class);
        //startActivity(intent);

        //Lanzar esperando 1 resultado
        //Intent intent = new Intent(this, TestActivity.class);
        //startActivityForResult(intent,1); //1 es el key

        //Intent intent = new Intent(this, TestActivity.class);
        //startActivity(intent); //1 es el key
    }

    private void contacts() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    readContacts();
                }
            }).start();

        } else {
            requestPermissions(
                    new String[]{Manifest.permission.READ_CONTACTS},
                    12);
        }
    }

    private void readContacts() {

        ArrayList<Contact> contacts = new ArrayList<>();
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {

            do {
                // get the contact's information
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                Integer hasPhone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                // get the user's email address
                String email = null;
                Cursor ce = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                if (ce != null && ce.moveToFirst()) {
                    email = ce.getString(ce.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    ce.close();
                }

                // get the user's phone number
                String phone = null;
                if (hasPhone > 0) {
                    Cursor cp = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (cp != null && cp.moveToFirst()) {
                        phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        cp.close();
                    }
                }

                // Get note
                String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                String[] noteWhereParams = new String[]{id,
                        ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE};
                Cursor noteCur = cr.query(ContactsContract.Data.CONTENT_URI, null, noteWhere, noteWhereParams, null);
                String note = null;
                if (noteCur.moveToFirst()) {
                    note = noteCur.getString(noteCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
                }
                noteCur.close();

                // Get relation
                String relationWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                String[] relationWhereParams = new String[]{id,
                        ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE};
                Cursor realtionCur = cr.query(ContactsContract.Data.CONTENT_URI, null, relationWhere, relationWhereParams, null);
                String relation = null;
                if (realtionCur.moveToFirst()) {
                    relation = realtionCur.getString(realtionCur.getColumnIndex(ContactsContract.CommonDataKinds.Relation.NAME));
                }
                realtionCur.close();


                if ((!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
                        && !email.equalsIgnoreCase(name)) || (!TextUtils.isEmpty(phone))) {
                    Contact contact = new Contact();
                    contact.name = name;
                    contact.email = email;
                    contact.phoneNumber = phone;
                    contact.note = note;
                    contact.relation = relation;
                    contacts.add(contact);
                }

            } while (cursor.moveToNext());

            // clean up cursor
            cursor.close();
        }

        int i = 0;


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
          /*  if (resultCode == RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.


            }*/
            Toast.makeText(this, "prueba: " + resultCode, Toast.LENGTH_SHORT).show();
        }
    }

    private void setupDrawer() {

        //Unused, see MaterialDrawer docs
        BadgeStyle badgeStyle = new BadgeStyle(Color.RED, Color.RED).withTextColor(Color.WHITE);

        /**
         * Creamos los ítems que se verán en el menú lateral (de momento básicos, como Primary y Secondary)
         *
         * Primary: Es el principal tipo de ítem. Es el más usado para poner cada opción principal en el menú latera (aka navdrawer).
         *          Este ítem puede contener badges (explicado más hacia delante), imágenes y texto
         *
         *          Ítem hijo de AbstractBadgeableDrawerItem.
         *
         *          Texto: para añadir texto utilizamos la funcion .withName(String|@StringRes) sobre un ítem hijo de AbstractBadgeableDrawerItem (Primary y Secondary).
         *          Imágen: para añadir una imágen utilizamos la función withIcon(Drawable|@DrawableRes) sobre un ítem hijo de AbstractBadgeableDrawerItem (Primary y Secondary).
         *                  Utilizamos la funcion withIconTintingEnabled para que cuando seleccionemos un ítem su imagen cambie de color
         *          Badges: badges son los cuadrados laterales con un número dentro. Por norma general este número indica una notificación. Es algo indicativo de que tienes algo dentro que no lo has visto nunca.
         *
         *                  Para crear un badge tenemos que utilizar la funcion withBadge(String) y para darle estilo (cambiar colores) utilizamos withBadgeStyle(BadgeStyle).
         *                  A la hora de crear un BadgeStyle utilizamos: new BadgeStyle(int color, int pressedColor).withTextColor(int color);
         *
         *
         * Secondary: funcionan exactamente igual que Primary, excepto que tienen un layout distinto, más pequeño. Estos ítems son idoneos para los ítems de "Ajustes" o "Ayuda"
         */
        //Contenedores
        relativeItem = new PrimaryDrawerItem().withName("RelativeLayout").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true);
        PrimaryDrawerItem linearItem = new PrimaryDrawerItem().withName("LinearLayout").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true);
        PrimaryDrawerItem scrollView = new PrimaryDrawerItem().withName("ScrollView").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true);
        //Controles
        PrimaryDrawerItem checkItem = new PrimaryDrawerItem().withName("CheckBox").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem radioButtonItem = new PrimaryDrawerItem().withName("RadioButton").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem spinnerItem = new PrimaryDrawerItem().withName("Spinner").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem cardItem = new PrimaryDrawerItem().withName("CardView").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem progressItem = new PrimaryDrawerItem().withName("ProgressBar").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem seekItem = new PrimaryDrawerItem().withName("SeekBar").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem switchItem = new PrimaryDrawerItem().withName("Switch").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem editItem = new PrimaryDrawerItem().withName("EditText").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem fabItem = new PrimaryDrawerItem().withName("FAB").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem calendarItem = new PrimaryDrawerItem().withName("CalendarView").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem hourView = new PrimaryDrawerItem().withName("HourView").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        //Interfaz
        PrimaryDrawerItem tabsItem = new PrimaryDrawerItem().withName("Tabs").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true);
        PrimaryDrawerItem recyclerView = new PrimaryDrawerItem().withName("RecyclerView").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true);
        PrimaryDrawerItem fragmentsItem = new PrimaryDrawerItem().withName("Fragments").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true);
        PrimaryDrawerItem navItem = new PrimaryDrawerItem().withName("NavigationView").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true);
        PrimaryDrawerItem bottombarItem = new PrimaryDrawerItem().withName("BottomBar").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true);

        //Notifications
        PrimaryDrawerItem notStatusBar = new PrimaryDrawerItem().withName("NotificationManager").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem dialog = new PrimaryDrawerItem().withName("Dialog").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem snackbarItem = new PrimaryDrawerItem().withName("SnackBar").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);

        //Librerias
        PrimaryDrawerItem libButterKnife = new PrimaryDrawerItem().withName("ButterKnife").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem libGlide = new PrimaryDrawerItem().withName("Glide").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);
        PrimaryDrawerItem libGson = new PrimaryDrawerItem().withName("GSON").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);

        //Mapas
        PrimaryDrawerItem maps = new PrimaryDrawerItem().withName("GoogleMaps").withIcon(R.drawable.ic_checkbox_blank_circle).withIconTintingEnabled(true);

        //Intents
        PrimaryDrawerItem intent = new PrimaryDrawerItem().withName("Intents").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true).withBadgeStyle(new BadgeStyle().withColor(Color.RED)).withBadge(".");
        //Animations
        PrimaryDrawerItem anim = new PrimaryDrawerItem().withName("Animations").withIcon(R.drawable.ic_checkbox_blank).withIconTintingEnabled(true).withBadgeStyle(new BadgeStyle().withColor(Color.RED)).withBadge(".");

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new SecondaryDrawerItem().withName("Contenedores").withSelectable(false), //0
                        relativeItem,//1
                        linearItem,//2
                        scrollView,//3
                        new DividerDrawerItem(),//4
                        new SecondaryDrawerItem().withName("Controles").withSelectable(false),//5
                        checkItem,//6
                        radioButtonItem,//7
                        spinnerItem,//8 //adapter
                        cardItem,//9
                        progressItem,//10
                        seekItem,//11
                        switchItem,//12
                        editItem,//13
                        fabItem,//14
                        calendarItem,//15
                        hourView,//16
                        new DividerDrawerItem(),//17
                        new SecondaryDrawerItem().withName("Interfaz").withSelectable(false),//18
                        tabsItem,//19
                        recyclerView,//20
                        fragmentsItem,//21
                        navItem,//22
                        bottombarItem,//23
                        new DividerDrawerItem(), //24
                        new SecondaryDrawerItem().withName("Notificaciones").withSelectable(false), //25
                        notStatusBar, //26
                        dialog, //27
                        snackbarItem, //28
                        new DividerDrawerItem(), //29
                        new SecondaryDrawerItem().withName("Librerias").withSelectable(false), //30
                        libButterKnife, //31
                        libGlide, //32
                        libGson, //33
                        new DividerDrawerItem(), //34
                        new SecondaryDrawerItem().withName("Mapas").withSelectable(false), //35
                        maps, //36
                        new DividerDrawerItem(), //37
                        new SecondaryDrawerItem().withName("Intents").withSelectable(false), //38
                        intent,//39
                        new DividerDrawerItem(), //40
                        new SecondaryDrawerItem().withName("Animaciones").withSelectable(false), //41
                        anim //42
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        changeScreen(position);
                        //Toast.makeText(MainActivity.this, "Posición pulsada #" + position, Toast.LENGTH_SHORT).show();
                        return false;
                    }
                })
                .build();
        result.setSelection(seekItem);
    }

    private void changeScreen(int pos) {
        Fragment fragment = null;

        switch (pos) {
            case 1:
                fragment = new RLFragment();
                toolbar.setTitle("RelativeLayout");
                break;
            case 2:
                fragment = new LLFragment();
                toolbar.setTitle("LinearLayout");
                break;
            case 3:
                fragment = new SVFragment();
                toolbar.setTitle("ScrollView");
                break;
            case 6:
                fragment = new CBFragment();
                toolbar.setTitle("CheckBox");
                break;
            case 7:
                fragment = new RBFragment();
                toolbar.setTitle("RadioButton y RadioGroup");
                break;
            case 8:
                fragment = new SpinnerFragment();
                toolbar.setTitle("Spinner");
                break;
            case 9:
                fragment = new CDFragment();
                toolbar.setTitle("CardView");
                break;
            case 10:
                fragment = new PBFragment();
                toolbar.setTitle("ProgressBar");
                break;
            case 11:
                fragment = new SBFragment();
                toolbar.setTitle("SeekBar");
                break;
            case 12:
                fragment = new SwitchFragment();
                toolbar.setTitle("Switch");
                break;
            case 13:
                fragment = new ETFragment();
                toolbar.setTitle("EditText");
                break;
            case 14:
                fragment = new FabFragment();
                toolbar.setTitle("FloatingActionButton");
                break;
            case 15:
                fragment = new CalendarFragment();
                toolbar.setTitle("CalendarView");
                break;
            case 16:
                fragment = new HourViewFragment();
                toolbar.setTitle("HourView");
                break;
            case 19:
                //tabs
                startActivity(new Intent(MainActivity.this, TabsActivity.class));
                break;
            /*case 20:
                fragment = new RecyclerViewClass();
                toolbar.setTitle("RecyclerView");
                break;*/
            case 20:
                fragment = new RecyclerViewGridClass();
                toolbar.setTitle("RecyclerGridView");
                break;
            case 22:
                startActivity(new Intent(MainActivity.this, NavDrawer.class));
                break;
            case 23:
                //bottombar
                startActivity(new Intent(MainActivity.this, BottomBarActivity.class));
                break;
            case 26:
                //notifications status bar
                fragment = new NotifFragment();
                toolbar.setTitle("NotificationManager");
                break;
            case 27:
                openDialog();
                break;
            case 28:
                openSnackBar();
                break;
            case 31:
                //butterknife
                break;
            case 32:
                //glide
                toolbar.setTitle("Glide");
                break;
            case 33:
                //Gson
                startActivity(new Intent(MainActivity.this, GsonLibrary.class));
                break;
            case 36:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(MainActivity.this, GoogleMapsActivity.class));
                } else {
                    requestPermissions(
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            11);
                }
                break;
            case 39:
                startActivity(new Intent(this, IntentsActivity.class));
                break;
            case 42:
                startActivity(new Intent(this, AnimationsActivity.class));
                break;
            case 0:
            case 4:
            case 5:
            case 17:
            case 18:
            case 24:
            case 25:
            case 29:
            case 30:
            case 34:
            case 35:
            case 37:
            case 38:
            case 40:
            case 41:
            default:
                break;
        }

        if (fragment != null) {
            try {
                FragmentTransaction transaction =
                        getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment, fragment);
                transaction.commit();
            } catch (Exception ignore) {
            }
        }
    }

    private void openDialog() {
        new AlertDialog.Builder(this/*, R.style.MyDialog*/)
                .setTitle("Esto es el titulo de un dialogo")
                .setMessage("Esto es el mensaje de un dialogo")
                .setPositiveButton("Siguiente", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Snackbar.make(coordinator, "Has pulsado el botón positivo", Snackbar.LENGTH_LONG).setAction("Mostrar", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                openSelectDialog();
                            }
                        }).show();
                    }
                })
                .setNeutralButton("DialogBoxConView", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(MainActivity.this, "Botón neutro pulsado", Toast.LENGTH_SHORT).show();
                        openSelectDialogView();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Snackbar snack = Snackbar.make(coordinator, "Has pulsado el botón negativo", Snackbar.LENGTH_LONG);
                        View view = snack.getView();
                        //color de fondo
                        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.md_white_1000));
                        //color del texto
                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snack.show();
                    }
                }).show();

        /*
        Para crear dialogos personalizados solo tendremos que añadir las lineas:
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v =inflater.inflate(R.layout.layout_to_inflate, null);
        builder.setView(v);
         */

    }

    private void openSelectDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Escoger color")
                .setItems(R.array.colores, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Snackbar.make(coordinator, "Posición seleccionada: " + which, Snackbar.LENGTH_SHORT).show();
                    }
                }).show();
    }

    private void openSelectDialogView() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.setTitle("Escoger color");


        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogbox, null);
        CheckBox chkTest = (CheckBox) v.findViewById(R.id.chkTest);
        chkTest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(MainActivity.this, "Seleccionado? " + String.valueOf(isChecked), Toast.LENGTH_SHORT).show();
            }
        });
        builder.setView(v);

        builder.show();
    }

    private void openSnackBar() {
        //coordinator = contenedor principal del activity en el que esto
        Snackbar.make(coordinator, "Esto es un ejemplo", Snackbar.LENGTH_LONG).setAction("Más", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "Botón más pulsado", Toast.LENGTH_SHORT).show();

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Enviar a"));
            }
        }).setActionTextColor(Color.YELLOW).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case 11: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(MainActivity.this, GoogleMapsActivity.class));
                } else {
                    Toast.makeText(MainActivity.this, "Es necesario activar la ubicación para utilizar esta función", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case 12: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    contacts();
                } else {
                    Toast.makeText(MainActivity.this, "Es necesario dar permisos para leer los contactos", Toast.LENGTH_SHORT).show();
                }
            }
            break;

        }
    }
}

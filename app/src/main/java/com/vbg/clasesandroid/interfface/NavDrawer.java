package com.vbg.clasesandroid.interfface;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import com.vbg.clasesandroid.MainActivity;
import com.vbg.clasesandroid.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by victo on 12/05/2017.
 */

public class NavDrawer extends AppCompatActivity {

    @Bind(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;

    Drawer result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupDrawer();
    }

    private void setupDrawer() {
        /**
         * Creamos los ítems de la lista
         */
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName("Primary Item");
        PrimaryDrawerItem item3 = new PrimaryDrawerItem().withName("Primary Item");
        PrimaryDrawerItem item4 = new PrimaryDrawerItem().withName("Primary Item");
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withName("Secondary Item");

        /**
         * Creamos el menú
         */
        result = new DrawerBuilder()
                //añadimos la actividad
                .withActivity(this)
                //añadimos la toolbar (para poder ubicar el botón, aka hamburger icon)
                .withToolbar(toolbar)
                //añadimos los ítems
                .addDrawerItems(
                        item1,
                        item3,
                        item4,
                        new DividerDrawerItem(),
                        item2,
                        new SecondaryDrawerItem().withName("Secondary Item"),
                        new SecondaryDrawerItem().withName("Cerrar activity")
                )
                //añadimos un listener para cuando se pulsa uno de los ítems del menu lateral
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (position != 6) {
                            Toast.makeText(NavDrawer.this, "Posición pulsada #" + position, Toast.LENGTH_SHORT).show();
                        } else {
                            onBackPressed();
                        }
                        return false;
                    }
                })
                //creamos el menu
                .build();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}

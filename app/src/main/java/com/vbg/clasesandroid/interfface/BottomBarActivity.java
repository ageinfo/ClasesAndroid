package com.vbg.clasesandroid.interfface;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.vbg.clasesandroid.R;
import com.vbg.clasesandroid.interfface.fragmentsexample.OneFragment;
import com.vbg.clasesandroid.interfface.fragmentsexample.ThreeFragment;
import com.vbg.clasesandroid.interfface.fragmentsexample.TwoFragment;

/**
 * Created by victo on 25/05/2017.
 */

public class BottomBarActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottombar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                int section = 0;
                switch (tabId) {
                    case R.id.tab_home:
                        section = 0;
                        break;
                    case R.id.tab_messages:
                        section = 1;
                        break;
                    case R.id.tab_profile:
                        section = 2;
                        break;
                    default:
                        break;
                }
                changeSection(section);
            }
        });
    }

    private void changeSection(int section) {

        Fragment fragment = null;
        switch (section) {
            case 0:
                fragment = new OneFragment();
                break;
            case 1:
                fragment = new TwoFragment();
                break;
            case 2:
                fragment = new ThreeFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            try {
                FragmentTransaction transaction =
                        getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment, fragment);
                transaction.commit();
            } catch (Exception ignore) {
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

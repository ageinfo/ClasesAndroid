package com.vbg.clasesandroid.interfface;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.vbg.clasesandroid.MainActivity;
import com.vbg.clasesandroid.R;
import com.vbg.clasesandroid.interfface.model.ItemObjects;
import com.vbg.clasesandroid.interfface.model.LengProg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victo on 12/05/2017.
 */

public class RecyclerViewGridClass extends Fragment {

    View rootView;
    private ArrayList<LengProg> lengProgs = new ArrayList<>();
    CustomAdapter customAdapter;
    RecyclerView recyclerView;
RelativeLayout rl;
    ViewGroup containeractual;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTheme(R.style.CheckBoxBlack);
        rootView = inflater.inflate(R.layout.rv_layout, container, false);
        containeractual  = container;

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv);
        rl= (RelativeLayout) rootView.findViewById(R.id.rl);

        // recyclerView.setLayoutManager(new LinearLayoutManager(getContext())); //Puede ser vertical o hori.
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        //recyclerView.setHasFixedSize(true); //indicas q es el mismo tamaño para todos los elementos

        recyclerView.setAdapter(new SolventRecyclerViewAdapter(getContext(), getListItemData()));

        //customAdapter = new CustomAdapter();

        //recyclerView.setAdapter(customAdapter);

        /*LengProg lengProg = new LengProg("Java", "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Duke_%28Java_mascot%29_waving.svg/226px-Duke_%28Java_mascot%29_waving.svg.png", 9.78);
        LengProg lengProg2 = new LengProg("PHP", "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/711px-PHP-logo.svg.png", 8.34);
        LengProg lengProg3 = new LengProg("C++", "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/C_plus_plus.svg/100px-C_plus_plus.svg.png", 1.78);
        LengProg lengProg4 = new LengProg("C", "https://3.bp.blogspot.com/-NWWGlBcgtSM/V6tB2TjL5PI/AAAAAAAABs4/NMV0aXWxJHk2576rQ0K5bnuP81o2LSQUACPcB/s1600/lenguaje-de-programacion-c.jpg", 5.00);
        LengProg lengProg5 = new LengProg("Perl", "https://upload.wikimedia.org/wikipedia/commons/f/f0/Cebolla_Chulita.png", 9.12);
        LengProg lengProg6 = new LengProg("JavaScript", "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/512px-Unofficial_JavaScript_logo_2.svg.png", 7.96);


        lengProgs.add(lengProg);
        lengProgs.add(lengProg2);
        lengProgs.add(lengProg3);
        lengProgs.add(lengProg4);
        lengProgs.add(lengProg5);
        lengProgs.add(lengProg6);

        customAdapter.notifyDataSetChanged(); //NOtifica al adapter q ha cambiado y  provoca que se actualice
        */
        openSnackBar();

        return rootView;
    }

    private void openSnackBar() {
        //coordinator = contenedor principal del activity en el que esto
        Snackbar.make(containeractual, "Esto es un ejemplo", Snackbar.LENGTH_LONG).setAction("Más", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( getContext(), "Botón más pulsado", Toast.LENGTH_SHORT).show();
            }
        }).setActionTextColor(Color.YELLOW).show();
    }

    private class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {


        class ViewHolder extends RecyclerView.ViewHolder {

            TextView score, name;
            ImageView img;
            CardView cardView;
            Button button2, button3;

            //Esqueleto del item (enlazar cada uno de sus controleS)
            ViewHolder(View itemView) {
                super(itemView);

                score = (TextView) itemView.findViewById(R.id.score);
                name = (TextView) itemView.findViewById(R.id.name);
                img = (ImageView) itemView.findViewById(R.id.image);
                cardView = (CardView) itemView.findViewById(R.id.card_view);
                button2 = (Button) itemView.findViewById(R.id.button2);
                button3 = (Button) itemView.findViewById(R.id.button3);
            }
        }

        @Override
        public CustomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_rv, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final CustomAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
            final LengProg lengProg = lengProgs.get(position);
            holder.score.setText(String.valueOf(lengProg.getScore()));
            holder.name.setText(lengProg.getName());

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "Tarjeta pulsada #" + position, Toast.LENGTH_SHORT).show();
                }
            });

            holder.button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "buton 2 pulsado", Toast.LENGTH_SHORT).show();
                }
            });

            holder.button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "buton 3 pulsado", Toast.LENGTH_SHORT).show();
                }
            });

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    Glide.with(RecyclerViewGridClass.this).load(lengProg.getSrc()).into(holder.img); //Solo para cargar na imagen de URL
                }
            });

        }

        @Override
        public int getItemCount() {
            return lengProgs.size();
        }

    }

    public class SolventRecyclerViewAdapter extends RecyclerView.Adapter<SolventRecyclerViewAdapter.SolventViewHolders> {

        public class SolventViewHolders extends RecyclerView.ViewHolder {

            public TextView countryName;
            public ImageView countryPhoto;

            public SolventViewHolders(View itemView) {
                super(itemView);
                countryName = (TextView) itemView.findViewById(R.id.country_name);
                countryPhoto = (ImageView) itemView.findViewById(R.id.country_photo);
            }
        }

        private List<ItemObjects> itemList;
        private Context context;

        public SolventRecyclerViewAdapter(Context context, List<ItemObjects> itemList) {
            this.itemList = itemList;
            this.context = context;
        }

        @Override
        public SolventViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.solvent_list, null);
            SolventViewHolders rcv = new SolventViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(SolventViewHolders holder, int position) {
            holder.countryName.setText(itemList.get(position).getName());
            holder.countryPhoto.setImageResource(itemList.get(position).getPhoto());
        }

        @Override
        public int getItemCount() {
            return this.itemList.size();
        }
    }

    private List<ItemObjects> getListItemData() {
        List<ItemObjects> listViewItems = new ArrayList<>();
        listViewItems.add(new ItemObjects("Uno", R.drawable.uno));
        listViewItems.add(new ItemObjects("Dos", R.drawable.dos));
        listViewItems.add(new ItemObjects("Tres", R.drawable.tres));
        listViewItems.add(new ItemObjects("Cuatro", R.drawable.cuatro));
        listViewItems.add(new ItemObjects("Uno", R.drawable.uno));
        listViewItems.add(new ItemObjects("Dos", R.drawable.dos));
        listViewItems.add(new ItemObjects("Tres", R.drawable.tres));
        listViewItems.add(new ItemObjects("Cuatro", R.drawable.cuatro));
        listViewItems.add(new ItemObjects("Uno", R.drawable.uno));
        listViewItems.add(new ItemObjects("Dos", R.drawable.dos));
        listViewItems.add(new ItemObjects("Tres", R.drawable.tres));
        listViewItems.add(new ItemObjects("Cuatro", R.drawable.cuatro));
        listViewItems.add(new ItemObjects("Uno", R.drawable.uno));
        listViewItems.add(new ItemObjects("Dos", R.drawable.dos));
        listViewItems.add(new ItemObjects("Tres", R.drawable.tres));
        listViewItems.add(new ItemObjects("Cuatro", R.drawable.cuatro));
        listViewItems.add(new ItemObjects("Uno", R.drawable.uno));
        listViewItems.add(new ItemObjects("Dos", R.drawable.dos));
        listViewItems.add(new ItemObjects("Tres", R.drawable.tres));
        listViewItems.add(new ItemObjects("Cuatro", R.drawable.cuatro));
        listViewItems.add(new ItemObjects("Uno", R.drawable.uno));
        listViewItems.add(new ItemObjects("Dos", R.drawable.dos));
        listViewItems.add(new ItemObjects("Tres", R.drawable.tres));
        listViewItems.add(new ItemObjects("Cuatro", R.drawable.cuatro));
        listViewItems.add(new ItemObjects("Uno", R.drawable.uno));
        listViewItems.add(new ItemObjects("Dos", R.drawable.dos));
        listViewItems.add(new ItemObjects("Tres", R.drawable.tres));
        listViewItems.add(new ItemObjects("Cuatro", R.drawable.cuatro));


        return listViewItems;
    }
}

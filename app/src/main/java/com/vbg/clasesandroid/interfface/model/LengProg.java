package com.vbg.clasesandroid.interfface.model;

/**
 * Created by victo on 12/05/2017.
 */

public class LengProg {

    private String name, src;
    private double score;

    public LengProg(String name, String src, double score) {
        this.name = name;
        this.src = src;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}

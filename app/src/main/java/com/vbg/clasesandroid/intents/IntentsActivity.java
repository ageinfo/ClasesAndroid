package com.vbg.clasesandroid.intents;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.view.Gravity;
import android.view.View;

import com.vbg.clasesandroid.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by victo on 20/07/2017.
 */

public class IntentsActivity extends AppCompatActivity {

    /**
     * Intents más comunes: https://developer.android.com/guide/components/intents-common.html?hl=es-419
     */


    @OnClick(R.id.share_btn)
    public void shareBtn(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Texto a mandar");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Compartir con"));
    }

    @OnClick(R.id.maps_btn)
    public void mapsBtn(View view) {
        Uri geoLocation = Uri.parse("geo:47.6,-122.3");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @OnClick(R.id.alarm_btn)
    public void alarmBtn(View view) {
        /*
        No olvidar el permiso <uses-permission android:name="com.android.alarm.permission.SET_ALARM" />
         */
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, "Alarma de ejemplo")
                .putExtra(AlarmClock.EXTRA_HOUR, 18)
                .putExtra(AlarmClock.EXTRA_MINUTES, 11);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @OnClick(R.id.gmail_btn)
    public void gmailBtn(View view) {

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"mail1@gmail.com", "mail2@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Asunto");
        intent.putExtra(Intent.EXTRA_TEXT, "Mensaje");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intents);
        ButterKnife.bind(this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

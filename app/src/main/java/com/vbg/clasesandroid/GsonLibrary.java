package com.vbg.clasesandroid;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.vbg.clasesandroid.interfface.model.LengProg;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by victo on 22/06/2017.
 */

public class GsonLibrary extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        TextView textView = new TextView(this);
        textView.setText("Esta en los logs :)");
        textView.setTextColor(Color.BLACK);
        textView.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        textView.setLayoutParams(param);

        setContentView(textView);

        /*
        Json de ejemplo
        http://json.org/example.html

        {
        "widget": {
            "debug": "on",
            "window": {
                "title": "Sample Konfabulator Widget",
                "name": "main_window",
                "width": 500,
                "height": 500
           },
            "image": {
                "src": "Images/Sun.png",
                "name": "sun1",
                "hOffset": 250,
                "vOffset": 250,
                "alignment": "center"
            },
            "text": {
                "data": "Click Here",
                "size": 36,
                "style": "bold",
                "name": "text1",
                "hOffset": 250,
                "vOffset": 100,
                "alignment": "center",
                }
            }
        }
         */

        String exampleJson = "{\n" +
                "        \"widget\": {\n" +
                "            \"debug\": \"on\",\n" +
                "            \"window\": {\n" +
                "                \"title\": \"Sample Konfabulator Widget\",\n" +
                "                \"name\": \"main_window\",\n" +
                "                \"width\": 500,\n" +
                "                \"height\": 500\n" +
                "           },\n" +
                "            \"image\": {\n" +
                "                \"src\": \"Images/Sun.png\",\n" +
                "                \"name\": \"sun1\",\n" +
                "                \"hOffset\": 250,\n" +
                "                \"vOffset\": 250,\n" +
                "                \"alignment\": \"center\"\n" +
                "            },\n" +
                "            \"text\": {\n" +
                "                \"data\": \"Click Here\",\n" +
                "                \"size\": 36,\n" +
                "                \"style\": \"bold\",\n" +
                "                \"name\": \"text1\",\n" +
                "                \"hOffset\": 250,\n" +
                "                \"vOffset\": 100,\n" +
                "                \"alignment\": \"center\"\n" +
                "                }\n" +
                "            }\n" +
                "        }";

        //Vamos a obtener el dato que hay en widget->image->alignment
        try {
            JSONObject jsonObject = new JSONObject(exampleJson);
            JSONObject jsonObject1 = jsonObject.getJSONObject("widget");
            JSONObject jsonObject2 = jsonObject1.getJSONObject("image");
            String aligmnet = jsonObject2.getString("alignment");
            Log.d("GsonLibrary", "El valor de alignment es: " + aligmnet);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //Vamos a pasar de un objeto a json, y de json a un objeto

        //Pasar de Objeto a Json es tan sencillo como
        //Nos creamos un objeto de prueba (este objeto lo utilizamos en la clase de RecyclerView)
        LengProg lengProg = new LengProg("Java", "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Duke_%28Java_mascot%29_waving.svg/226px-Duke_%28Java_mascot%29_waving.svg.png", 9.78);
        String jsonObject = new Gson().toJson(lengProg);
        Log.d("GsonLibrary", "Objeto pasado a JSON: " + jsonObject);

        //Ahora pasamos de json a objeto
        //El primer parámetro es el json que queremos pasar y el segundo la estructura del objeto
        //Para poder visualizar que tha ido bien, ponemos un breakpoint para poder debugar y ver lo que contiene el objeto
        //comprobamos que el objeto leng y lengProg son iguales, si lo son es que todo ha ido genial
        LengProg leng = new Gson().fromJson(jsonObject, LengProg.class);
        int i = 3;

        /**
         * Como pasar arraylist (o listas) a json y viceversa:
         *
         * .fromJson(lista, new TypeToken<Lista<Objeto>>(){}.getType());
         */


    }
}
